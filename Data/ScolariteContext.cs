﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApiV1.Models;

    public class ScolariteContext : DbContext
    {
        public ScolariteContext (DbContextOptions<ScolariteContext> options)
            : base(options)
        {
        }

        public DbSet<WebApiV1.Models.Groupe>? Groupe { get; set; }

        public DbSet<WebApiV1.Models.Etudiant>? Etudiant { get; set; }

        public DbSet<WebApiV1.Models.Inscription>? Inscription { get; set; }

        public DbSet<WebApiV1.Models.Matiere>? Matiere { get; set; }
    }
